Notes on fabrication of the Noisebox enclosures
===============================================

- [Notes on fabrication of the Noisebox enclosures](#notes-on-fabrication-of-the-noisebox-enclosures)
  - [General Info:](#general-info)
  - [3D printing:](#3d-printing)
    - [Tolerances:](#tolerances)
  - [Lasercutting](#lasercutting)
  - [Basic laser cutting procedure for printing at CIRMMT:](#basic-laser-cutting-procedure-for-printing-at-cirmmt)
    - [1. Fusion 360](#1-fusion-360)
    - [2. Illustrator (see note at end)](#2-illustrator-see-note-at-end)
    - [3. CorelDraw (laser cutter workstation at CIRMMT)](#3-coreldraw-laser-cutter-workstation-at-cirmmt)
    - [4. Laser cutter](#4-laser-cutter)
      - [4a. Trotec Job Control and laser cutter:](#4a-trotec-job-control-and-laser-cutter)

## General Info: 

For all Noisebox instruments - Keybox (aka Noisebox v3), Stringbox, and Tapbox - CAD is done using Autodesk Fusion 360, for which free academic licenses are available to McGill students. Fusion 360 is cloud-based, so I can share the project with others set up with software and account.

The bodies of the instruments themselves are fabricated with a frame made of 3D printed parts and lasercut acrylic panels. Additional parts (mounting brackets, etc.) have also been fabricated with the same techniques. 

Most of the fabrication is done at CIRMMT, which has a small fablab containing 3D printers and a lasercutter. For info on whats there, see https://booking.cirmmt.org/items/prototyping/. 


The Stringbox frame was printed using IDMIL's Prusa i3 mk3s 3D printer, and utilizes a different design strategy. Where the frames of the other printer are one printed as a single piece (taking advantage of the larger available printing volume with the CIRMMT 3D printers), the Stringbox frame is created out of several pieces which slide together and are secured in place by the lasercut panels and screws. In theory, this method could be leveraged towards more easily replicable (and changeable) designs, as all pieces can be printed using inexpensive hobbyist-level 3D printers, and the basic design of the parts can be reused and easily modified to accommodate other shapes and sizes. See the Stringbox Fusion 360 files to see how everything goes together. 

## 3D printing: 

Printer choice: Prusa i3 (IDMIL) vs Stratasys uPrint/Fortus (CIRMMT)

- Prusa PLA is many times cheaper than Stratasys ABS. 
- Finish smoothness is very similar, though the Prusia is worse with printing at odd angles etc. 
- Stratasys seems overall stronger. For more information about the Stratasys printers, see the printer documentation at https://booking.cirmmt.org/items/prototyping/

There is also a resin 3D printer at CIRMMT; this is especially useful for printing small parts in very high resolution, though this is not really applicable for the Noisebox instruments and hasn't been used for this project. 

### Tolerances: 

- Prusa i3: For pieces sliding into one another, if the outer sleeve has a thickness of 1mm, the inner piece should be shelled 1.075 for adequate clearance. 
- Stratasys: 1.1mm shell or slightly large seems to be better. 

## Lasercutting

Screw hole dimensions and countersink:

- #4 assembly screws:
    - screw hole: 2.5mm diameter
    - countersink: 6mm diameter, 3mm deep.
- M2.5 component screws:
    + screw hole: 2.5mm
    + countersink: 5mm diameter, 2.25mm deep
- *#6 mounting screws: DONT USE, TOO BIG*
    + *screw hole: 3.75mm*
    + *countersink: 7mm diameter, 3mm deep*

Laser cutter params: 

- Colors: 
    + light etch (lettering) - black #000000
    + first cut - red #ff0000
    + deep etch (3mm countersink) - blue #0000ff
    + second cut - teal #336699
- Make sure Illustrator is set to RGB color mode, not CMYK. If it is CMYK it will change all the colors and Trotec Job Control won't recognize them. 
- There is a correct setup in Job Control Materials now: CIRMMT > Acrylic 6mm Noisebox (this sets the laser power and speed settings for each layer/color)

---

## Basic laser cutting procedure for printing at CIRMMT: 

The basic procedure involves 4 distinct steps each outlined below: 

1. Exporting a 2D file from your CAD design
2. Preparing the file so it can be read and understood by the lasercutter printing software
3. Importing the prepared file into the computer (at CIRMMT) from which the print will be launched
4. Setting up the laser cutter and printing software; starting the print

### 1. Fusion 360

- Create a single sketch with all cuts and etches; omit any other measuring lines, etc. For more complex cutting it is usually easier to make a new sketch and trace features from previous layers. 
- Export sketch as a .dxf file. 

### 2. Illustrator (see note at end)

- Position sketch on an artboard the size and orientation of your acrylic panel. 
- !IMPORTANT!: Set color mode (File > Document Color Mode) to RGB. 
- Create a layer for each type of cut/etch. Use the following colors and set stroke thickness to 0.001 pt. for cuts: 
    + light etch (lettering) - black #000000
    + first cut - red #ff0000 
    + deep etch (3mm countersink) - blue #0000ff
    + second cut - teal #336699 (use this for outer cuts)
- Also check for duplicate lines and shapes and delete them. 
- Can put multiple pieces on the same artboard, separated into layers. 
- Save as regular .ai file. Put on USB drive to bring to CIRMMT computer

### 3. CorelDraw (laser cutter workstation at CIRMMT)

- Open .ai file in CorelDraw. 
- Check to ensure that all colors are assigned correctly (correct colors, hairline cuts, etc.)
- If you have multiple parts to print, you can turn off each layer you DON'T want to print with the Enable/Unenable print icon (little printer icon on each layer), and only leave the layer you want to print on. The print popup will show 1 Issue (Visible but non-printable layers) but this is okay. 
- Select Print in the File menu, Select 'Trotec' as printer and then open Preferences. Check the following settings: 
    + Set 'Material Settings' to correct preset (Acrylic 6mm Noisebox for now)
    + Process Options: halftone = Color
- Then click JC to send to printer. 

### 4. Laser cutter

- Ensure laser cutter is on and turn on ventilation fan if not already running. 
- Load material in laser cutter. 
- Move print bed up and use the little widget to set the correct distance between the tip and material. 
- Position the laser tip at the desired top left location of your cut. 

#### 4a. Trotec Job Control and laser cutter: 

- Your job should be on the menu on right. Drag it onto the cut bed. 
- CTL-I for WYSIWYG 
- Under Calculation, check that the cutting steps are set correctly, click Update to update time. 
- If needed update Material Settings in Settings Menu. 
- Click on USB icon to connect to laser cutter. This should give you crosshairs of where the laser tip is. Click and drag your piece to snap to the laser location. 
- When everything is correct, click the print button. 

_**Note: Adobe Illustrator isn't free. A good free alternative is Inkscape, however it is not nearly as user friendly and you will have to do some tests to ensure that everything works correctly. One very important item to check is that the units of measurement are consistent between the software (e.g, if your Inkscape units are mm, when it goes to Corel Draw and/or Trotec Job Control) it much recognize the units as mm or else your print will be comically large or small..) It sounds trivial but this happens quite a bit._

_Also, you can skip Illustrator/Inkscape and import/prepare the .dxf file directly in CorelDraw (if you have your own version, or on the lasercutter computer at CIRMMT). My choice for Illustrator is based on a) I've used Adobe for years and know it well, and b) in my tests it is the quickest and most streamlined workflow._