Noisebox work winter 2021
=========================

Objectives: Update the (3) current instruments. Create new copies of each using the [Bela](https://bela.io) platform. 

## Timeline: 

| Instrument | Version | Time to complete | Work to do |
| - |:-:|:-:| - |
| Tapbox | T1 | 5d | coding, revise piezo mounts |
|        | T2 | 5d | build new version |
| Keybox | K1 | 2 - 4d | code cleanup and testing | 
|        | K2 | 10d | build new version for Bela | 
| Stringbox | S1 | 10d | pickup redesign, coding | 
|        | S2 | 9 - 11d | build new version for Bela | 

---

### Instrument 1: Tapbox

(T1 = existing instrument; T2 = instrument duplicate)

- Objectives:: Software for T1, sensor revisions for T1, construct T2
- **Timeline:: ~10d to complete 2 instruments**
    - Complete first instrument (2.5d)
        - Finish code (home)
    - Revisions and tests: (2.5d)
        - Design new piezo mounts/holders and revise signal acquisition software (IDMIL)
    - Construct a copy (5d)
        - CAD redesign/revisions? (IDMIL) or (home) (1d)
        - Fabrication - 3D printing and laser cutting at (CIRMMT) (1d)
        - Electronics assembly and construction at (IDMIL) (1.5d)
        - Software installation and testing at (IDMIL) or (home) (1.5d)

### Instrument 2: Keybox

(K1 = existing instrument; K2 = instrument duplicate)

- Objectives:: Software update for K1, design/build K2 Bela version
- **Timeline:: ~13d to complete 2 instruments**
    - First instrument (K1) refresh (2 - 4d)
        - Testing and tweaks (.5d)
        - IMU/FX mapping (1d)
        - Final testing (.5d)
        - Maybe need more time depending on tests? (1 - 2d)
    - Create a Bela version (10d)
        - Hardware:: (4d)
            - Revise MCAD for Bela (.5d)
            - Revise ECAD for Bela (.5d)
            - Fabrication - 3D printing and laser cutting at (CIRMMT) (1d)
            - Electronics assembly/construction/tests at (IDMIL) (2d)
        - Software:: (4d)
            - **Important: Read IMU/i2c sensors directly on Bela to bypass Arduino/Teensy** (2d)
                - This is doable but requires some learning and experimentation:
                    - use Arduino or other C++ lib to extract usable sensor data, then send to SC via OSC
                    - or write/find a SC Ugen that does it <-- better way. 
                    - or finally, port the entire instrument code to C++ (go through Andrew's course [C++ Real-Time Audio Programming with Bela](https://www.youtube.com/watch?v=aVLRUyPBBJk&list=PLCrgFeG6pwQmdbB6l3ehC8oBBZbatVoz3))
                - See https://forum.bela.io, in particular [this thread](https://forum.bela.io/d/276-reading-mpr121-in-supercollider) for a starting point, and these other [posts](https://forum.bela.io/?q=imu%20supercollider)
            - Port K1 software to Bela (2d)
        - Final assembly and testing:: (2d)

### Instrument 3: Stringbox

(S1 = existing instrument; S2 = instrument duplicate)

- Objectives:: S1 redesign pickup system, develop sequencer mode, test; create S2 Bela version
- **Timeline:: ~20d to complete 2 instruments**
    - S1 revisions and completion:: (10d) 
        - S1 pickup redesign (3d)
        - S1 uke mode w/ pickup mapping (1d)
        - S1 sequencer mode (4d)
        - S1 test and complete (2d)
    - S2 create Bela version:: (9 - 11d) 
        - Hardware:: (4d)
            - Revise/redesign MCAD for Bela (2d)
            - Revise/redesign ECAD for Bela (2d)
        - Software:: (2d)
            - Port S1 software to Bela (2d) 
                - See "**Important: Read IMU/i2c sensors directly on Bela to bypass Arduino/Teensy** (2d)"
        - Assembly and testing:: (3 - 5d)
