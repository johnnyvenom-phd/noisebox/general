# Noisebox general project stuff

![teaser](instruments_teaser.jpg)

----

**UPDATE: FEB 2021:** Noisebox design work resumes, see [winter2021_notes.md](winter2021_notes.md) for details.

----

This repo contains notes and other materials related to the Noisebox project. There are multiple other repositories containing the project files for the various instruments: 

- Noisebox v3: the latest iteration of the Noiseboxes
- Stringbox: a guitar-like DMI based on the Noisebox
- Tapbox: a digital, "mini-cajon" instrument
- Noisebox v2: previous versions of the Noisebox designed in 2016/17

The Noiseboxes are a family of instruments I began designing during my MFA research after I arrived at the [IDMIL](idmil.org) as a visiting researcher. I wrote a [paper](http://www-new.idmil.org/publication/noisebox-design-and-prototype-of-a-new-digital-musical-instrument/) for the 2015 ICMC conference about the initial design. 

Since then I have built several different Noiseboxes as prototypes for research in user-centered design and evaluation of digital musical instruments (DMIs). 

The current designs are part of my Ph.D. thesis research "Built to Perform: Designing Digital Musical Instruments for Active Use", based on a series of co-design workshops with active and professional musicians. 

Also working on this project are Patrick Cowden and Julian Vanasse, as part of their master research in music technology at McGill. 
