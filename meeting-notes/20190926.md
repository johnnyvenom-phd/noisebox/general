Noisebox meeting - Fall 2019
============================

26-September 2019  
IDMIL, McGill University

## Collaborators: 

Julian - julian.vanasse@mail.mcgill.ca  
Patrick - patrick.cowden@mail.mcgill.ca

Both are in Marcelo's MUMT 620 class, and will use this project towards their final projects. 

——

## What we did: 

- overview of entire project
- 3 instruments - Noisebox, Stringbox, Tapbox
- Overview of the tech: 
    - Prynth version (Noisebox)
    - COBS version (Stringbox - COBS)
    
## Potential Projects: 

- Julian: 
    - SC code: Stringbox
    - Developing new instrument (Tapbox)
- Patrick: 
    - Learning SC
    - Rotary encoders

## Next: 

- set up GitLab accounts
- clean up repo (me)
- check out repo, issues, and codebase
- Meet Tuesday 1-Oct, 2:30pm 
    - set projects, scope, timelines, outcomes
    - decide on work schedule