work session with Julian and Patrick:
=====================================

Friday, Oct 4th, 2019  
IDMIL

## Julian: 

Wrote a SC Karplus Strong patch to put on Stringbox. 
Working with Stringbox sensors sending data from Teensy to laptop (SC IDE) to get that working, then can put it all to the RPi. 

Things to check/fix: (johnny's WIP notes)

- Teensy code: 
    - wasn't outputting data to RPi as previously set up. Why? 
    - Check versions: I was last working on [v_1](Stringbox/teensy/stringbox_teensy_cobs_1), that could alternately send to PC. 
    - Try original version?  
    - Ok, currently running on RPi with the following files/config:
        - Teensy code: `stringbox_teensy_cobs`
            - `serialDebug = true` or `false` (both working correctly)
            - `send = false` (serial send triggered from SC)
        - SC code: `stringbox_remote_default.scd` --> `serialDataReceive.scd`

Ok, everything is now working with updated Teensy code (stringbox_teensy_cobs) and updated controls.scd. 

Can run in RPi or PC, just need to change the serialDebug in Teensy to false (RPi only) or true (output for PC)

### Next steps: 

- working example of Sringbox sensors playing KS synth on PC, then we can add it to the RPi code. 


```
// start RPi serial port SC prompt: 
p = SerialPort("/dev/ttyAMA0", stopbit: false)
```

-------

## Patrick

...has working example of 4 rotary encoders working well.  
Now we are looking for a solution to map speed of turn to increment value., ie., the faster the user turns the larger the increment and vice versa. 

See Gitlab issues for links to some libraries that might have this built in. 

Going through some SC tutorials and will build a basic synth controlled by the 4 rotary encoders

### Next steps: 

- MWE (minimum working example) of rotary encoders controlling SC synth
- Investigate Prynth Arduino code and see how to incorporate encoder code into it. 

